package fr.univlille.iut.info.r402;


public enum PizzaEnum {

    REINE("JAMBON","CHAMPIGNON","MOZARELLA","TOMATE"),
    MARGHERITA("MOZARELLA","TOMATE","BASILIC"),
    QUATREFROMAGE("MOZARELLA","EMMENTAL");
    
    
    
    
    private Ingredients[] listeIngredients;
    
    private PizzaEnum(String... ingredients){
        listeIngredients= new Ingredients[ingredients.length];
        int cpt=0;
        for (String string : ingredients) {
            string=string.toUpperCase();
            listeIngredients[cpt]=Ingredients.valueOf(string);
            cpt++;
        }
    }

    public Ingredients[] getIngredients(){
        return this.listeIngredients;
    }
    
    
    

}
