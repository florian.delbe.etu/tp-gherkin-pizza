package fr.univlille.iut.info.r402;

public class Pizzaiolo {
        public Pizza preparePizza(String nom){
            switch (nom.toUpperCase()) {
                case "REINE":
                    return new Pizza(PizzaEnum.REINE.getIngredients());
                case "MARGHERITA":
                    return new Pizza(PizzaEnum.MARGHERITA.getIngredients());
                default:
                return null;
                    
            }
        }
        
}
