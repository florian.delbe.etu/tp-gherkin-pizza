package fr.univlille.iut.info.r402;

import java.util.ArrayList;


public class Four {
    ArrayList<Pizza> enAttente;

    public Four(){
        enAttente=new ArrayList<Pizza>();
    }

    public void mettreEnAttente(Pizza pizza){
        enAttente.add(pizza);
    }
    public void mettreACuire(Pizza pizza,int temps){
        try {
            Thread.sleep(temps*1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}
