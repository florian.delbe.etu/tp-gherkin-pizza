package fr.univlille.iut.info.r402;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PizzaStepdefs {
    Four four;
    Pizzaiolo mario;
    Pizza pizza;
    @Given("^un four")
    public void unFour() {
        four=new Four();
    }

    @And("^un pizzaiolo")
    public void unPizzaiolo() {
        mario= new Pizzaiolo();
    }

    @And("^le pizzaiolo prépare une pizza reine")
    public void preparerReine() {
        pizza= mario.preparePizza("reine");
    }
    @When("^le pizzaiolo met la pizza reine au four$")
    public void pizzaAuFour() {
        four.mettreEnAttente(pizza);
    }

    @Then("au bout de {int} ticks d'horloge, la pizza est cuite")
    public void pizzaCuite(int ticks) {
        four.mettreACuire(pizza, ticks);
    }

}
